package game.world 
{
	import game.entities.Enemy;
	import game.entities.Player;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;

	public class GameWorld extends World
	{
		
		public var player:Player;
		public var enemies:Vector.<Enemy> = new Vector.<Enemy>();
		public var wave:int = 0;
		
		public var killcount:int = 0;
		
		public var killtext:Text;
		
		public function GameWorld() 
		{
			trace("Vi är i GameWorld");
			
			Globals.gameWorld = this;
			
			
			player = new Player();
			add(player);

			var e:Entity = new Entity();
			killtext = new Text(killcount.toString());
			e.graphic = killtext;
			killtext.color = 0x000000;
			e.x = 5;
			e.y = 2;
			add(e);
			
		}
		
		public function addEnemy(enemy:Enemy):void 
		{
			// lägg till fiend i vår värld
			add(enemy);
			// lägg till fiend i vår värld
			enemies.push(enemy);
		}
		
		
		public function removeEnemy(enemy:Enemy):void
		{
			// ta bort från vår värld
			remove(enemy);
			// ta bort fiendin från vår array
			for (var i:int = enemies.length - 1; i >= 0; i--)
			{
				if (enemies[i] == enemy) {
					enemies.splice(i, 1);
					break;
				}
			}
		}
		
		override public function update():void
		{
			killtext.text = killcount.toString();
			if (enemies.length <= 0) {
				wave++;
				
				for (var i:int = 0; i < wave; i++ ) {
				addEnemy(new Enemy());
				}
			}
			
			
			super.update();
		}
	}

}