package game.entities 
{
	import game.entities.weapons.Weapon;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	
	
	public class Laser extends Entity
	{
		
		public var weapon:Weapon;
		
		public var speed:Number = 7;
		
		
		public var dirX:Number;
		public var dirY:Number;
		
		public var damage:Number = 1;
		
		public var friction:Number = 0.96;
		public var time:int = 12;
		
		
		public function Laser(_weapon:Weapon, _x:int, _y:int, _dirX:Number, _dirY:Number)
		{
			
			super(_x, _y);
			
			weapon = _weapon;
			
			dirX = _dirX;
			dirY = _dirY;
			
			
			graphic = Image.createRect(20, 6, 0x00ff00);
			graphic.x -= 0;
			graphic.y -= 3;
			
			(graphic as Image).angle = Math.atan2(dirY, dirX) * FP.DEG;
			
			setHitbox(20, 20, 3, 3);
			
		}
		
		override public function update():void
		{
			
			x += dirX * speed;
			y += dirY * speed;
			speed *= friction;
			
			if (speed <= 2) {
				remove();	
			}
			
			var hitEnemy:Enemy = collide("enemy", x, y) as Enemy;
			if (hitEnemy != null) {
				remove();
				hitEnemy.health -= damage;
			}
			
		}
		
		public function remove():void
		{
			
			world.remove(this);
			
		}
		
		
	}

}