package game.entities 
{
	import game.entities.weapons.Weapon;
	import net.flashpunk.graphics.Image;

	public class BulletGrenade extends Bullet
	
	{
		
		public function BulletGrenade(_weapon:Weapon,_x:int, _y:int, _dirX:Number, _dirY:Number) 
		{
			super(_weapon,_x, _y, _dirX, _dirY);
			
			graphic = Image.createRect(12, 12, 0x0000FF);
			graphic.x -= 6;
			graphic.y -= 6;
		
			setHitbox(12, 12, 6, 6);
			
			time = 5;
			speed = 3;
		}
		
		override public function remove():void
		{
			for (var i:int = 0; i < 50; i++ ) {
			/* 
			//skit spridning
			var b:Bullet = new Bullet(weapon, x, y, - 1 + 2 * Math.random(), - 1 + 2 * Math.random());
			b.speed = 7;
			*/
			
			//bra spridnin
			var a:Number = i / 58 * Math.PI * 2;
			var b:Bullet = new Bullet(weapon, x, y,Math.cos(a),Math.sin(a));
			weapon.owner.world.add(b);
			
			b.speed = 5;
			b.damage = 3;
			}
			
			super.remove();
		}
	}

}