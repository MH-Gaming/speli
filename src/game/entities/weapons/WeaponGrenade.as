package game.entities.weapons 
{
	import game.entities.Bullet;
	import game.entities.BulletGrenade;
	import net.flashpunk.Entity;

	public class WeaponGrenade extends Weapon
	{
		
		
		public function WeaponGrenade(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			cooldown = 30;
		}
		
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// lägg till skote till världen
			owner.world.add(new BulletGrenade(this, owner.x, owner.y, dirX, dirY));
			
			super.shoot(dirX, dirY);
		}
		
	}

}