package game.entities.weapons 
{
	import game.entities.Bullet;
	import game.entities.Player;
	import net.flashpunk.Entity;

	public class WeaponShotgun extends Weapon
	{
		
		public function WeaponShotgun(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			cooldown = 5;
		}
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// lägg till skote till världen
			for (var i:int = 0; i < 5; i++ ){
			owner.world.add(new Bullet(this, owner.x, owner.y, dirX - 0.3 + 0.6*Math.random(), dirY - 0.3 + 0.6*Math.random()));
			}
			super.shoot(dirX, dirY);
		}
	}
}	