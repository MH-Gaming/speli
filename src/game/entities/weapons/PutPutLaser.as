package game.entities.weapons 
{
	import game.entities.Bullet;
	import game.entities.Laser;
	import net.flashpunk.Entity;

	public class PutPutLaser extends Weapon
	{
		public function PutPutLaser(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			cooldown = 7;	
		}
		override public function shoot(dirX:Number, dirY:Number):void
		{
		
			// lägg till skote till världen
			owner.world.add(new Laser(this, owner.x, owner.y, dirX, dirY));
			
			super.shoot(dirX, dirY);
		
		}
	}
}