package game.entities.weapons 
{
	import game.entities.Bullet;
	import net.flashpunk.Entity;

	public class WeaponPistol extends Weapon
	{
		
		public function WeaponPistol(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			cooldown = 10;
		}
		
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// lägg till skote till världen
			owner.world.add(new Bullet(this, owner.x, owner.y, dirX, dirY));
			
			super.shoot(dirX, dirY);
		}
	}

}