package 
{
	import game.world.GameWorld;
	import net.flashpunk.Engine;
	import net.flashpunk.FP;

	[SWF(width = 640, height = 480, frameRate = 30, backgroundColor=0xFFFFFF)]
	public class Main extends Engine 
	{
		
		public function Main():void 
		{
			super(640, 480, 30);
			
		}

		override public function init():void 
		{
			// byt bakgrundsfärg
			FP.screen.color = 0xFFFFFF;

			// starta en ny värld
			FP.world = new GameWorld();
		}
		
	}
	
}